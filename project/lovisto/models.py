from django.db import models

# Create your models here.
class Content (models.Model):
    title = models.CharField(max_length=128)
    url = models.TextField()
    description = models.TextField()
    user = models.TextField()
    likes = models.IntegerField()
    dislikes = models.IntegerField()
    comments = models.IntegerField()
    date = models.DateTimeField()
    info = models.TextField()

    def __str__(self):
        return str(self.user) + ": " + self.title

class Comment (models.Model):
    content = models.ForeignKey(Content, on_delete = models.CASCADE)
    body = models.TextField()
    date = models.DateTimeField()
    user = models.TextField()

    def __str__(self):
        return self.content.title + ": " + self.body

class Vote (models.Model):
    TYPE = [
        (1, 'like'),
        (0, 'dislike')
    ]
    content = models.ForeignKey(Content, on_delete = models.CASCADE)
    user = models.TextField()
    value = models.IntegerField(choices=TYPE)

    def __str__(self):
        return self.user + ": " + self.content.title + ": " + str(self.value)