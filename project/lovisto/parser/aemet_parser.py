#!/usr/bin/python3
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string
import urllib
import urllib.error
import urllib.request

def max_min(self, name):
    if name == 'maxima':
        self.inContent = True
    elif name == 'minima':
        self.inContent = True

def endContent(self):
    self.content = ""
    self.inContent = False

class Handler(ContentHandler):

    def __init__(self):
        self.inContent = False
        self.Day = False
        self.Temp = False
        self.Sens = False
        self.Hum = False
        self.content = ""
        self.tempMax = ""
        self.tempMin = ""
        self.sensMax = ""
        self.sensMin = ""
        self.humMax = ""
        self.humMin = ""
        self.date = ""
        self.days = []
        self.info = {}

    def startElement(self, name, attrs):

        if name == 'nombre':
            self.inContent = True
        elif name == 'provincia':
            self.inContent = True
        elif name == 'copyright':
            self.inContent = True
        elif name == 'dia':
            self.date = attrs.get('fecha')
            self.Day = True

        elif self.Temp:
            max_min(self, name)

        elif self.Sens:
            max_min(self, name)

        elif self.Hum:
            max_min(self, name)

        elif self.Day:
            if name == 'temperatura':
                self.Temp = True
            elif name == 'sens_termica':
                self.Sens = True
            elif name == 'humedad_relativa':
                self.Hum = True

    def endElement(self, name):
        global provincia

        if name == 'nombre':
            self.info['municipio'] = self.content
            endContent(self)

        elif name == 'provincia':
            self.info['provincia'] = self.content
            provincia = self.content
            endContent(self)

        elif name == 'copyright':
            self.info['copyright'] = self.content
            endContent(self)

        elif name == 'dia':
            self.Day = False
            self.days.append({'tempMin': self.tempMin,
                              'tempMax': self.tempMax,
                              'sensMin': self.sensMin,
                              'sensMax': self.sensMax,
                              'humMin': self.humMin,
                              'humMax': self.humMax,
                              'fecha': self.date})

        elif name == 'temperatura':
            self.Temp = False
        elif name == 'sens_termica':
            self.Sens = False
        elif name == 'humedad_relativa':
            self.Hum = False
        elif self.Temp:
            if name == 'maxima':
                self.tempMax = self.content
            elif name == 'minima':
                self.tempMin = self.content

            endContent(self)

        elif self.Sens:
            if name == 'maxima':
                self.sensMax = self.content
            elif name == 'minima':
                self.sensMin = self.content

            endContent(self)

        elif self.Hum:
            if name == 'maxima':
                self.humMax = self.content
            elif name == 'minima':
                self.humMin = self.content

            endContent(self)

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars

def aemetparser(url):
    id_num = url.split("-id", maxsplit=1)[1]

    Parser = make_parser()
    hand = Handler()
    Parser.setContentHandler(hand)
    url = "https://www.aemet.es/xml/municipios/localidad_"+ id_num +".xml"
    xmlStream = urllib.request.urlopen(url)
    Parser.parse(xmlStream)

    return (hand.info['municipio'], hand.info['provincia'], hand.info['copyright'], hand.days)

def aemet(url):
    (municipio, provincia, copyright, days_info) = aemetparser(url)
    url = "https://" + url

    template = '<div class="aemet">\
    <p>Datos AEMET para ' + municipio + ' (' + provincia + ')</p>\
    <ul>'
    for i in range(0, len(days_info)):
        template = template + '<li>' + days_info[i]['fecha'] + '. Temperatura: ' + days_info[i]['tempMin'] \
                   + '/' + days_info[i]['tempMax'] + ', sensación: ' + days_info[i]['sensMin'] \
                   + '/' + days_info[i]['sensMax'] + ', humedad: ' + days_info[i]['humMin'] + '/' + days_info[i]['humMax'] + '.</li>'
    template = template + '\
    </ul>\
    <p>' + copyright + '</p>.\
    <p><a href="' + url + '">Página original en AEMET</a></p>\
    </div>'

    return template