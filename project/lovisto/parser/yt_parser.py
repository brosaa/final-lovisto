#!/usr/bin/python3

import json
import urllib
import urllib.error
import urllib.request

def ytparser(url):
    ident = url.split("watch?v=", maxsplit=1)[1]

    url = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + ident
    response = urllib.request.urlopen(url)
    jsonStream = json.loads(response.read())

    return (jsonStream['title'], jsonStream['author_name'], jsonStream['html'])

def youtube(url):
    (title, author, iframe) = ytparser(url)
    url = "https://" + url
    template = '<div class="youtube">\
    <p>Video YouTube: ' + title + '</p>' + iframe + '\
    <p>Autor: ' + author + '.</p>\
    <a href="' + url + '">Video en YouTube</a></p>\
    </div>'

    return template