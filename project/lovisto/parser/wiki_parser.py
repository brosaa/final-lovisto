#!/usr/bin/python3

import json
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib
import urllib.error
import urllib.request
import json

text = ""

class TextHandler(ContentHandler):

    def __init__(self):
        self.content = ""
        self.inContent = False
        self.textInfo = {}
        self.id = ""

    def startElement(self, name, attrs):

        if name == 'extract':
            self.inContent = True
        if name == 'page':
            self.id = attrs.get('_idx')

    def endElement(self, name):
        global text
        global a

        a = self.id
        if name == 'extract':
            self.textInfo['text'] = self.content
            text = self.content
            self.inContent = False
            self.content = ""

    def characters(self, chars):
        if self.inContent:
            if len(chars) < 400:
                self.content = self.content + chars
            else:
                self.content = self.content + chars[:400] #maximo 400 caracteres

        #return self.handler.textInfo, self.img_info

def wikiparser(url):

    path = url.split("/")
    site = path[-1]

    text_url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + site + "&prop=extracts&exintro&explaintext"
    img_url = "https://es.wikipedia.org/w/api.php?action=query&titles=" + site + "&prop=pageimages&format=json&pithumbsize=100"

    textStream = urllib.request.urlopen(text_url)
    imgStream = urllib.request.urlopen(img_url)

    Parser = make_parser()
    hand = TextHandler()
    Parser.setContentHandler(hand)

    Parser.parse(textStream)

    jsonStream = json.load(imgStream)
    return (site, hand.textInfo['text'], jsonStream['query']["pages"][hand.id]["thumbnail"]["source"])

def wiki(url):
    (title, text, image) = wikiparser(url)
    url = "https://" + url
    template = '<div class="wikipedia">\
    <p>Artı́culo Wikipedia: ' + title + '</p>\
    <p><img src="' + image + '"></p>\
    <p>"' + text + '"</p>\
    <p><a href="' + url + '">Artı́culo original</a>.</p>\
    </div>'

    return template