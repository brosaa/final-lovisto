#!/usr/bin/python3
from bs4 import BeautifulSoup
import urllib.request

def htmlParser(url):
    result = ""
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')

    title = soup.find('meta', property='og:title')
    if title:
        title = title["content"]
        result = "og"
    else:
        title = soup.title.string
        result = "title"

    existImage = soup.find('meta', property='og:image')
    if existImage:
        image = existImage["content"]
        result = "og"
    else:
        image = existImage

    return (title, image, result)

def html(url):
    (title, image, result) = htmlParser(url)
    if result == "og":
        template = '<div class ="og"><p>' + title + '</p>'

        if image:
            template = template + '<img src = "' + image + '" width="300" height="300" >'

        template = template + '</div>'
    elif result == "title":
        template = '<div class ="html">\
                <p>' + title + '</p>'
    else:
        template = '<div class ="html">\
                        <p>Información extendida no disponible</p>'

    template = template + '<p><a href="' + url + '">Página original</a>.</p>\
    </div>'
    return template