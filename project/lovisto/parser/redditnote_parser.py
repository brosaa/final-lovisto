#!/usr/bin/python3

import json
import urllib
import urllib.error
import urllib.request

def redditparser(url):
    subreddit = url.split('/')[4]
    ident = url.split('/')[6]

    urljson = "https://www.reddit.com/r/" + subreddit + "/comments/" + ident + "/.json"
    response = urllib.request.urlopen(urljson)
    jsonStream = json.load(response)
    info = jsonStream[0]['data']['children'][0]['data']

    return (info['title'], info['subreddit'], info['selftext'], info['upvote_ratio'], info['url'])

def redditnote(url):
    (title, subreddit, text, approbe, link) = redditparser(url)

    if link.startswith("https://i.redd.it/"):
        template = '<div class="reddit">\
            <p>Nota Reddit: ' + title + '</p>\
            <img src="' + link + '">\
            <p><a href="' + url + '">Publicado en ' + subreddit + '</a>. Aprobación: ' + approbe + '.</p>\
            </div>'
    else:
        template = '<div class="reddit">\
            <p>Nota Reddit: ' + title + '</p>\
            <p>' + text + '</p>\
            <p><a href="' + url + '">Publicado en ' + subreddit + '</a>. Aprobación: ' + str(approbe) + '.</p>\
            </div>'
    return template