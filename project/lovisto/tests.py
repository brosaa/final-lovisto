from django.test import TestCase

from .parsers import get_info

from .parser.wiki_parser import wikiparser
from .parser.aemet_parser import aemetparser
from .parser.yt_parser import ytparser
from .parser.redditnote_parser import redditparser
from .parser.html_parser import htmlParser

from django.test import Client
from django.contrib.auth.models import User

# Create your tests here.

class ParsersTest(TestCase):
    def test_ParserHtml(self):
        url = "https://www.holaislascanarias.com/"
        (title, image, result) = htmlParser(url)
        self.assertEquals(title, "Hola desde el mejor clima del mundo")
        self.assertEquals(image, "https://www.holaislascanarias.com/sites/default/files/styles/16_9_desktop/public/header-images/2021-05/cab-home.jpg?itok=1k7KBE1r")

    def test_parserYoutube(self):
        url = "https://www.youtube.com/watch?v=IfoSqaxJsAM"
        (title, author, iframe) = ytparser(url)
        self.assertEquals(title, "Presentación de la asignatura. Back-end y front-end.")
        self.assertEquals(author, "CursosWeb")
        self.assertIn("accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;", iframe)

    def test_parserAemet(self):
        url = "http://www.aemet.es/es/eltiempo/prediccion/municipios/getafe-id28065"
        (municipio, provincia, copyright, days_info) = aemetparser(url)
        self.assertEquals(municipio, "Getafe")
        self.assertEquals(provincia, "Madrid")
        self.assertIn("citando a AEMET", copyright)

    def test_parserWikipedia(self):
        site = "https://es.wikipedia.org/wiki/Astronauta"
        (title, text, image) = wikiparser(site)
        self.assertEquals(title, "Astronauta")
        self.assertIn("cosmonauta o taikonauta es el término que designa a", text)
        self.assertEquals(image, "https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Bruce_McCandless_II_during_EVA_in_1984.jpg/100px-Bruce_McCandless_II_during_EVA_in_1984.jpg")

    def test_parserReddit(self):
        site = "https://www.reddit.com/r/django/comments/n842st/is_there_a_public_repo_that_shows_productionlevel/"
        (title, subreddit, text, approbe, link) = redditparser(site)
        self.assertEquals(title, "Is there a public repo that shows production-level code of django models?")
        self.assertIn("maybe if there is anything out there. I'm a self-taught developer doing freelancing", text)


class GetTest(TestCase):

    def test_homePage(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)

        content = response.content.decode('utf-8')
        self.assertIn("Lo Visto - Página principal", content)

    def test_userPage(self):
        client = Client()

        user = User.objects.create(username='user')
        user.set_password('abcdefg')
        user.save()

        log = client.login(username='user', password='abcdefg')
        self.assertTrue(log)

        response = client.get('/user/')
        self.assertTemplateUsed(response, "lovisto/userPage.html")


    def test_allPublications(self):
        client = Client()
        response = client.get('/publications/')
        content = response.content.decode('utf-8')
        self.assertIn("Lo Visto - Publicaciones", content)



