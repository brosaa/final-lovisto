from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.template import loader

from .parsers import get_info
from urllib.parse import urlparse
from django.utils import timezone
from .models import Content, Comment, Vote

from django import forms

class ContentForm(forms.ModelForm):
    class Meta:
        model = Content
        fields = ('title', 'url', 'description')

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)

def addNewVote(value, content, username):
    userVote = Vote.objects.all().filter(content=content).filter(user=username)
    if not userVote:
        if value == 1:
            content.likes = content.likes + 1
            vote = Vote(content=content, user=username, value=1)
        else:
            content.dislikes = content.dislikes + 1
            vote = Vote(content=content, user=username, value=0)

        vote.save()
        content.save()
    else:
        userVote = Vote.objects.get(content=content, user=username)
        if value == 1:
            if userVote.value == 0:
                content.dislikes = content.dislikes - 1
                content.likes = content.likes + 1
                userVote.value = 1
                content.save()
                userVote.save()
        else:
            if userVote.value == 1:
                content.likes = content.likes - 1
                content.dislikes = content.dislikes + 1
                userVote.value = 0
                content.save()
                userVote.save()

def logUser(request):
    if request.POST['action'] == 'Logout':
        logout(request)
    if request.POST['action'] == 'Login':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)

def currentPage(request):
    current = str(request)
    current = current.split('/', maxsplit=1)[1]
    current = current.split("'>")[0]

    if current == "":
        return "home"
    else:
        return current[:-1]

def changeStyle(request):

    global style

    if request.POST['action'] == 'Modo oscuro':
        style = "dark"
    elif request.POST['action'] == 'Modo claro':
        style = "main"

# Create your views here.

style = ""

@csrf_exempt
def home(request):

    if request.method == "POST":
        logUser(request)
        if request.POST['action'] == 'New content':
            url = request.POST['url']
            title = request.POST['title']
            description = request.POST['description']

            try:
                content = Content.objects.get(title=title)
                content.delete()

            except Content.DoesNotExist:
                pass

            htmlContent = get_info(url)
            new = Content(title=title, url = url, description=description, user=request.user.username,
                          likes=0, dislikes=0, comments=0, date=timezone.now(), info=htmlContent)
            if request.user.is_authenticated:
                new.save()
        elif request.POST['action'] == 'like':
            title = request.POST['title']
            content = Content.objects.get(title=title)
            addNewVote(1, content, request.user.username)
        elif request.POST['action'] == 'dislike':
            title = request.POST['title']
            content = Content.objects.get(title=title)
            addNewVote(0, content, request.user.username)

        changeStyle(request)

    template = loader.get_template('lovisto/homePage.html')
    context = {'contents10': Content.objects.all().order_by('-date')[:10], 'form': ContentForm(),
               'contents5': Content.objects.all().filter(user=request.user.username).order_by('-date')[:5],
               'username': request.user.username, 'votes': Vote.objects.all(),
               'contents3': Content.objects.all().order_by('-date')[:3],
               'currentPage': currentPage(request), 'style': style}

    return HttpResponse(template.render(context, request))

def contentPage(request, title):

    try:
        content = Content.objects.get(title=title)
    except Content.DoesNotExist:
        return render(request, 'lovisto/notContet.html')

    if request.method == "POST":
        logUser(request)
        if request.POST['action'] == 'New comment':
            commentBody = request.POST['body']
            new = Comment(content = content, body = commentBody, date=timezone.now(), user=request.user.username)
            new.save()
            content.comments = content.comments + 1
            content.save()
        elif request.POST['action'] == 'like':
            addNewVote(1, content, request.user.username)
        elif request.POST['action'] == 'dislike':
            addNewVote(0, content, request.user.username)

        changeStyle(request)

    if Vote.objects.all().filter(content=content).filter(user=request.user.username):
        userVote = Vote.objects.get(content=content, user=request.user.username)
    else:
        userVote = None

    template = loader.get_template('lovisto/contentPage.html')
    context = { 'content': content,
                'form': CommentForm(),
                'comments': content.comment_set.all,
                'username': request.user.username,
                'vote': userVote,
                'contents3': Content.objects.all().order_by('-date')[:3],
               'currentPage': currentPage(request), 'style': style}

    return HttpResponse(template.render(context, request))

def userPage(request):

    if request.method == "POST":
        logUser(request)

        changeStyle(request)

    if request.user.is_authenticated:
        template = loader.get_template('lovisto/userPage.html')
        context = {'publications': Content.objects.all(),
                   'comments': Comment.objects.all(),
                   'votes': Vote.objects.all(),
                   'username': request.user.username,
                   'contents3': Content.objects.all().order_by('-date')[:3],
                    'currentPage': currentPage(request), 'style': style}
        return HttpResponse(template.render(context, request))
    else:
        return render(request, 'lovisto/notAuthenticated.html')

def allPublications(request):

    if request.method == "POST":
        logUser(request)

        changeStyle(request)

    if request.GET.get("format") == "json" or request.GET.get("format") == "json/":
        queryset = Content.objects.filter().values()
        return JsonResponse({"Publicaciones": list(queryset)})
    if request.GET.get("format") == "xml" or request.GET.get("format") == "xml/":
        template = loader.get_template('lovisto/allPublicationsXML.html')
        context = {'publications': Content.objects.all().order_by('-date')}
        return HttpResponse(template.render(context, request))
    else:
        template = loader.get_template('lovisto/allPublications.html')
        context = {'publications': Content.objects.all().order_by('-date'),
                   'username': request.user.username,
                   'contents3': Content.objects.all().order_by('-date')[:3],
                    'currentPage': currentPage(request), 'style': style}
        return HttpResponse(template.render(context, request))

def infoPage(request):

    if request.method == "POST":
        logUser(request)

        changeStyle(request)

    template = loader.get_template('lovisto/infoPage.html')
    context = {'username': request.user.username,
               'contents3': Content.objects.all().order_by('-date')[:3],
               'currentPage': currentPage(request), 'style': style}

    return HttpResponse(template.render(context, request))