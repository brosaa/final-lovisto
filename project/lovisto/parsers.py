from .parser.wiki_parser import wiki
from .parser.aemet_parser import aemet
from .parser.yt_parser import youtube
from .parser.redditnote_parser import redditnote
from .parser.html_parser import html

def iswiki(site, path):
    if (site=="es.wikipedia.org"):
        if (path.startswith("wiki")):
            return True

    return False

def isaemet(site, path):
    if (site == "www.aemet.es" or site == "aemet.es"):
        if (path.startswith("es/eltiempo/prediccion/municipios/")):
            if (path.split("-id", maxsplit=1)[1].isdigit()):
                return True

    return False

def isyt(site, path):
    if (site=="www.youtube.com" or site == "youtube.com"):
        if (path.startswith("watch?v=")):
            return True

    return False

def isreddit(site, path):
    if (site=="www.reddit.com" or site == "reddit.com"):
        if (path.find("r/") == -1 or path.find("/comments/") == -1):
            return False
        else:
            return True

    return False

def get_info(url):
    if url.startswith('https://') or url.startswith('http://'):
        url = url.split('//', maxsplit=1)[1]

    site = url.split('/', maxsplit=1)[0]
    path1 = url.split('/', maxsplit=1)[1]
    path = path1.split('/', maxsplit=1)[0]

    print("Site: " + site + ", path: " + path1)

    if iswiki(site, path):
        return wiki(url)
    elif isaemet(site, path1):
        return aemet(url)
    elif isyt(site, path1):
        return youtube(url)
    elif isreddit(site, path1):
        url = "http://" + url
        return redditnote(url)
    else:
        url = "http://" + url
        return html(url)
