from django.urls import path, re_path
from . import views
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage

urlpatterns = [
    path("", views.home),
    path('publications/', views.allPublications),
    path('publications/<str:title>', views.contentPage),
    path('user/', views.userPage),
    path('info/', views.infoPage),
]