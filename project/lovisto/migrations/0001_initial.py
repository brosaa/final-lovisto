# Generated by Django 3.1.7 on 2021-06-28 17:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('url', models.TextField()),
                ('description', models.TextField()),
                ('user', models.TextField()),
                ('likes', models.IntegerField()),
                ('dislikes', models.IntegerField()),
                ('comments', models.IntegerField()),
                ('date', models.DateTimeField()),
                ('info', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.TextField()),
                ('type', models.IntegerField(choices=[(1, 'like'), (0, 'dislike')])),
                ('content', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.content')),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('body', models.TextField()),
                ('date', models.DateTimeField()),
                ('user', models.TextField()),
                ('content', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lovisto.content')),
            ],
        ),
    ]
