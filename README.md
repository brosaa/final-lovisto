# Final Lo Visto

Práctica final del curso 2020/21

# Entrega practica
## Datos
* Nombre: Belén Rosa Alonso
* Titulación: Doble Grago: ITT + IAA
* Despliegue (url): http://brosaa.pythonanywhere.com/
* Video básico (url): https://youtu.be/KuAuO9V15Es
* Video parte opcional (url): https://youtu.be/vHs4-cCuXQI

## Cuenta Admin Site
* admin/admin
## Cuentas usuarios
* Belen/asignaturasat
* Diego/asignaturasat
* Esther/asignaturasat
## Resumen parte obligatoria
Práctica final de SAT: Aplicación web que gestiona aportaciones subidas por los usuarios, dichas aportaciones se pueden votar y comentar.
Si un usuario introduce una nueva aportación con el mismo título que una antigua, la antigua se borra y actualiza.

Para dicha aplicación, hemos usado Django, SQLite para bases de datos y PythonAnywhere para el despliegue.
## Lista partes opcionales
* Favicon
* Recursos reconocidos de los cuatro sitios del enunciado
* Iconos de like, dislike y comentarios en las páginas 
